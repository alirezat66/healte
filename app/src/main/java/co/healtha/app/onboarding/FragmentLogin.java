package co.healtha.app.onboarding;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import co.healtha.app.R;
import co.healtha.app.utils.BaseFragment;
import co.healtha.app.utils.view.CustomTextViewMedium;

public class FragmentLogin extends BaseFragment {

    private View rootView;
    private CustomTextViewMedium txtBtnLogin;
    public static FragmentLogin newInstance(Bundle bundle) {
        FragmentLogin fragment = new FragmentLogin();
        fragment.setArguments(bundle);
        return fragment;
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = setContentView(R.layout.fragment_login);
        txtBtnLogin = (CustomTextViewMedium) rootView.findViewById(R.id.btnlogin);
        txtBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getContext(),"salam",Toast.LENGTH_LONG).show();
            }
        });
        init();
        return rootView;
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    private void init() {

    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *

}
