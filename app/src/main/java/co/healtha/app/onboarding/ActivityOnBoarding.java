package co.healtha.app.onboarding;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import co.healtha.app.R;
import co.healtha.app.utils.BaseFragmentActivity;
import co.healtha.app.utils.Utility;
import co.healtha.app.utils.view.OnBoardingViewPager;
import me.relex.circleindicator.CircleIndicator;

public class ActivityOnBoarding extends BaseFragmentActivity {

    private OnBoardingViewPager viewPagerOnBoarding;
    private CircleIndicator indicatorOnBoarding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.setDirection(this);
        setContentView(R.layout.activity_on_boarding);


        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragmentMain);
      /*  if (fragment == null) {
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.fragmentMain, new FragmentOnboarding1());
            ft.commit();
        }
*/
        init();
    }

    private void init() {
        viewPagerOnBoarding = (OnBoardingViewPager) findViewById(R.id.viewPagerOnBoarding);
        indicatorOnBoarding = (CircleIndicator) findViewById(R.id.indicatorOnBoarding);

        Fragment[] fragments = new Fragment[6];
        fragments[0] = FragmentOnboarding1.newInstance(new Bundle());
        fragments[1] = FragmentOnboarding2.newInstance(new Bundle());
        fragments[2] = FragmentOnboarding3.newInstance(new Bundle());
        fragments[3] = FragmentOnboarding4.newInstance(new Bundle());
        fragments[4] = FragmentOnboarding5.newInstance(new Bundle());
        fragments[5] = FragmentOnboarding6.newInstance(new Bundle());

        AdapterOnBoardingViewPager adapter = new AdapterOnBoardingViewPager(getSupportFragmentManager(), fragments);
        viewPagerOnBoarding.setAdapter(adapter);

        //TODO set adapter for view pager

        indicatorOnBoarding.setViewPager(viewPagerOnBoarding);
    }
}
