package co.healtha.app.onboarding;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import co.healtha.app.R;
import co.healtha.app.utils.view.CircleProgressView;

public class ActivityDashboard extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        CircleProgressView circleCare = (CircleProgressView) findViewById(R.id.circleCare);
        circleCare.setMin(0);
        circleCare.setMaxInside(100);
        circleCare.setMaxOutside(100);
        circleCare.setProgressInsideWithAnimation(40);
        circleCare.setProgressOutsideWithAnimation(70);

    }
}
