package co.healtha.app.onboarding;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by ahmad on 1/16/17.
 */

public class AdapterOnBoardingViewPager extends FragmentPagerAdapter {

    private Fragment[] fragments;

    public AdapterOnBoardingViewPager(FragmentManager fm, Fragment[] fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int index) {
        return this.fragments[index];
    }

    @Override
    public int getCount() {
        return this.fragments.length;
    }

}

