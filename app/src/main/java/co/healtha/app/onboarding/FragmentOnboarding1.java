package co.healtha.app.onboarding;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.healtha.app.R;
import co.healtha.app.utils.BaseFragment;

public class FragmentOnboarding1 extends BaseFragment {

    private View rootView;

    public static FragmentOnboarding1 newInstance(Bundle bundle) {
        FragmentOnboarding1 fragment = new FragmentOnboarding1();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = setContentView(R.layout.fragment_onboarding_1);
        init();
        return rootView;
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    private void init() {

    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *

}
