package co.healtha.app.onboarding;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.healtha.app.R;
import co.healtha.app.utils.BaseFragment;

public class FragmentOnboarding3 extends BaseFragment {

    private View rootView;

    public static FragmentOnboarding3 newInstance(Bundle bundle) {
        FragmentOnboarding3 fragment = new FragmentOnboarding3();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = setContentView(R.layout.fragment_onboarding_3);
        init();
        return rootView;
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    private void init() {

    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *

}
