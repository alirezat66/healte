package co.healtha.app.onboarding;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.healtha.app.R;
import co.healtha.app.utils.BaseFragment;

public class FragmentPreQuestionAge extends BaseFragment {

    private View rootView;

    public static FragmentPreQuestionAge newInstance(Bundle bundle) {
        FragmentPreQuestionAge fragment = new FragmentPreQuestionAge();
        fragment.setArguments(bundle);
        return fragment;
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = setContentView(R.layout.fragment_pre_question_age);
        init();
        return rootView;
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    private void init() {

    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *

}
