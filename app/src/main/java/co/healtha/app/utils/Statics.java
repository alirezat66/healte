package co.healtha.app.utils;

import android.os.Environment;

import java.io.File;

/**
 * Created by ahmad on 1/20/17.
 */

public class Statics {

    public static final String TAG = "HealthADebug";
    public static final int CACHE_DISK_USAGE_BYTES = 200 * 1024 * 1024;  // 200 MB

// * * * * * * * * * * * * * * Volley configuration * * * * * *  * * * * * * * * * * *
    public static final int VOLLEY_TIMEOUT_MS = 10000;// 10 second
    public static final int VOLLEY_MAX_RETRIES = 3;
    public static final int VOLLEY_AUTH_MAX_RETRIES = 1;
    public static final float VOLLEY_BACKOFF_MULT = 1f;

// * * * * * * * * * * * * * * Files and Directories * * * * * * * * * * * * * * * * *
    public static final String SHOP_DIRECTORY = Environment.getExternalStorageDirectory() + File.separator + Utility.getContext();
    public static final String CACHE_DIRECTORY = SHOP_DIRECTORY + File.separator + "cache";

// * * * * * * * * * * * * * * * * * * API * * * * * * * * * * * * ** * * * * * * * * *
    public static final String URL_IP = "";
    public static final String URL_PREFIX = URL_IP + "/api/mobile/";


}
