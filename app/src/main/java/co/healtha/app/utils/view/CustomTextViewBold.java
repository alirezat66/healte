package co.healtha.app.utils.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

import co.healtha.app.utils.SharedPreferencesHelper;
import co.healtha.app.utils.Utility;

/**
 * Created by ahmad on 1/16/17.
 */

public class CustomTextViewBold extends TextView {

    public CustomTextViewBold(Context context) {
        super(context);
        if(!isInEditMode()) {
            init();
        }
    }

    public CustomTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()) {
            init();
        }
    }

    public CustomTextViewBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(!isInEditMode()) {
            init();
        }
    }

    public CustomTextViewBold(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if(!isInEditMode()) {
            init();
        }
    }

    private void init() {
        setTypeface(Utility.getBoldTypeFace());
        setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * SharedPreferencesHelper.getInstance().getFontSizeScale());
    }
}
