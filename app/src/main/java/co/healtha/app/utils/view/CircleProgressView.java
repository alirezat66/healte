package co.healtha.app.utils.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import co.healtha.app.R;
import co.healtha.app.utils.Utility;

/**
 * Created by ahmad on 2/2/17.
 */

public class CircleProgressView extends View {

    /**
     * ProgressBar's line thickness
     */
    private float strokeWidth = 4;
    private float strokeBackWidth = 4;
    private String backgroundColor = "#c3c3c3";
    private float progressInside = 60;
    private float progressOutside = 40;
    private int min = 0;
    private int maxInside = 100;
    private int maxOutside = 100;
    /**
     * Start the progress at 12 o'clock
     */
    private int startAngle = -90;
    private int colorInside = Color.DKGRAY;
    private int colorOutisde = Color.DKGRAY;
    private RectF rectFInside;
    private RectF rectFOutside;
    private Paint backgroundPaint;
    private Paint InsideForegroundPaint;
    private Paint OutsideForegroundPaint;

    public float getStrokeWidth() {
        return strokeWidth;
    }

    public void setStrokeWidth(float strokeWidth) {
        this.strokeWidth = strokeWidth;
        backgroundPaint.setStrokeWidth(strokeWidth);
        InsideForegroundPaint.setStrokeWidth(strokeWidth);
        OutsideForegroundPaint.setStrokeWidth(strokeWidth);
        invalidate();
        requestLayout();//Because it should recalculate its bounds
    }

    public float getProgressInside() {
        return progressInside;
    }

    public void setProgressInside(float progressInside) {
        this.progressInside = progressInside;
        invalidate();
    }

    public float getProgressOutside() {
        return progressOutside;
    }

    public void setProgressOutside(float progressOutside) {
        this.progressOutside = progressOutside;
        invalidate();
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
        invalidate();
    }

    public int getMaxOutside() {
        return maxOutside;
    }

    public void setMaxOutside(int maxOutside) {
        this.maxOutside = maxOutside;
        invalidate();
    }

    public int getMaxInside() {
        return maxInside;
    }

    public void setMaxInside(int maxInside) {
        this.maxInside = maxInside;
        invalidate();
    }

    public int getColorOutisde() {
        return colorOutisde;
    }

    public void setColorOutisde(int colorOutisde) {
        this.colorOutisde = colorOutisde;
        backgroundPaint.setColor(adjustAlpha(Color.parseColor(backgroundColor), 0.3f));
        backgroundPaint.setStrokeWidth(strokeBackWidth);
        OutsideForegroundPaint.setColor(colorOutisde);
        invalidate();
        requestLayout();
    }

    public int getColorInside() {
        return colorInside;
    }

    public void setColorInside(int colorInside) {
        this.colorInside = colorInside;
        backgroundPaint.setColor(adjustAlpha(Color.parseColor(backgroundColor), 0.3f));
        backgroundPaint.setStrokeWidth(strokeBackWidth);
        InsideForegroundPaint.setColor(colorInside);
        invalidate();
        requestLayout();
    }

    public CircleProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        rectFInside = new RectF();
        rectFOutside = new RectF();
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CircleProgressBar,
                0, 0);
        //Reading values from the XML layout
        try {
            strokeWidth = typedArray.getDimension(R.styleable.CircleProgressBar_progressBarThickness, strokeWidth);
            strokeBackWidth = strokeWidth;
            progressInside = typedArray.getFloat(R.styleable.CircleProgressBar_progressInside, progressInside);
            progressOutside = typedArray.getFloat(R.styleable.CircleProgressBar_progressOutside, progressOutside);
            colorInside = typedArray.getInt(R.styleable.CircleProgressBar_insideProgressbarColor, colorInside);
            colorOutisde = typedArray.getInt(R.styleable.CircleProgressBar_outsideProgressbarColor, colorOutisde);
            min = typedArray.getInt(R.styleable.CircleProgressBar_min, min);
            maxInside = typedArray.getInt(R.styleable.CircleProgressBar_maxInside, maxInside);
            maxOutside = typedArray.getInt(R.styleable.CircleProgressBar_maxOutside, maxOutside);
        } finally {
            typedArray.recycle();
        }

        backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundPaint.setColor(adjustAlpha(Color.parseColor(backgroundColor), 0.3f));
        backgroundPaint.setStyle(Paint.Style.STROKE);
        backgroundPaint.setStrokeWidth(strokeBackWidth);

        InsideForegroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        InsideForegroundPaint.setColor(colorInside);
        InsideForegroundPaint.setStyle(Paint.Style.STROKE);
        InsideForegroundPaint.setStrokeWidth(strokeWidth);

        OutsideForegroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        OutsideForegroundPaint.setColor(colorOutisde);
        OutsideForegroundPaint.setStyle(Paint.Style.STROKE);
        OutsideForegroundPaint.setStrokeWidth(strokeWidth);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //draw inside arc
        canvas.drawArc(rectFInside, 140, 260, false, backgroundPaint);
        float angleInside = 260 * progressInside / maxInside;
        canvas.drawArc(rectFInside, 140, angleInside, false, InsideForegroundPaint);

        //draw outside arc
        canvas.drawArc(rectFOutside, 140, 260, false, backgroundPaint);
        float angleOutside = 260 * progressOutside / maxOutside;
        canvas.drawArc(rectFOutside, 140, angleOutside, false, OutsideForegroundPaint);


        //draw inside circle
        int insideValue = 10;
        int angle = 260 * (insideValue - getMin()) / (getMaxInside() - getMin());
        float x = radius + (radius) * (float) Math.cos(-angle);
        float y = radius + (radius) * (float) Math.sin(-angle);
//        canvas.drawBitmap(icon, x, y, null);

//        Path path = new Path();
//        path.arcTo(rectFOutside, 140, angle, true);
//        canvas.drawPath(path, InsideForegroundPaint);
////        canvas.getClipBounds()
//        canvas.save();
        canvas.drawBitmap(icon, x + Utility.dp2px(6), y + Utility.dp2px(1), null);
    }
    Bitmap icon;
    float radius;

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        final int height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        final int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int min = Math.min(width, height);
        setMeasuredDimension(min, min);

        rectFOutside.set(0 + strokeWidth / 2, 0 + strokeWidth / 2, min - strokeWidth / 2, min - strokeWidth / 2);
        rectFInside.set(strokeWidth + strokeWidth / 2, strokeWidth + strokeWidth / 2, min - (strokeWidth / 2) - strokeWidth, min - (strokeWidth / 2) - strokeWidth);


        radius = min /2;
        Drawable drawable = getResources().getDrawable(R.drawable.dashboard_chart_circle_inside);
        icon = drawableToBitmap(drawable);
        icon = Bitmap.createScaledBitmap(icon, (int) (strokeWidth * 0.8), (int) (strokeWidth * 0.8), false);

    }

    /**
     * Lighten the given color by the factor
     *
     * @param color  The color to lighten
     * @param factor 0 to 4
     * @return A brighter color
     */
    public int lightenColor(int color, float factor) {
        float r = Color.red(color) * factor;
        float g = Color.green(color) * factor;
        float b = Color.blue(color) * factor;
        int ir = Math.min(255, (int) r);
        int ig = Math.min(255, (int) g);
        int ib = Math.min(255, (int) b);
        int ia = Color.alpha(color);
        return (Color.argb(ia, ir, ig, ib));
    }

    /**
     * Transparent the given color by the factor
     * The more the factor closer to zero the more the color gets transparent
     *
     * @param color  The color to transparent
     * @param factor 1.0f to 0.0f
     * @return int - A transplanted color
     */
    public int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    /**
     * Set the progress with an animation.
     * Note that the {@link ObjectAnimator} Class automatically set the progress
     * so don't call the {@link CircleProgressView#setProgressInside(float)} directly within this method.
     *
     * @param progress The progress it should animate to it.
     */
    public void setProgressInsideWithAnimation(float progress) {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, "progressInside", progress);
        objectAnimator.setDuration(1500);
        objectAnimator.setInterpolator(new DecelerateInterpolator());
        objectAnimator.start();
    }

    /**
     * Set the progress with an animation.
     * Note that the {@link ObjectAnimator} Class automatically set the progress
     * so don't call the {@link CircleProgressView#setProgressOutside(float)} directly within this method.
     *
     * @param progress The progress it should animate to it.
     */
    public void setProgressOutsideWithAnimation(float progress) {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, "progressOutside", progress);
        objectAnimator.setDuration(1500);
        objectAnimator.setInterpolator(new DecelerateInterpolator());
        objectAnimator.start();
    }
}