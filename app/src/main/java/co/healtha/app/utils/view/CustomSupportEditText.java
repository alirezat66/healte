package co.healtha.app.utils.view;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.View;

import co.healtha.app.utils.Statics;
import co.healtha.app.utils.Utility;

/**
 * Created by a.vatani on 1/25/17.
 */

public class CustomSupportEditText extends AppCompatEditText {

    private String errorText;
    private String requiredErrorText;
    public int status;
    private CustomTextInputLayout relatedTextInputLayout;

    public static final int DISABLE_STATE          = 0;
    public static final int DEFAULT_STATUS         = 1;
    public static final int OK_STATUS              = 2;
    public static final int ERROR_STATUS           = 3;
    public static final int REQUIRE_STATE          = 4;
    public static final int DISABLE_ERROR_STATE    = 5;

    public CustomSupportEditText(Context context) {
        super(context);
        if(!isInEditMode()) {
            init();
        }
    }

    public CustomSupportEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()) {
            init();
        }
    }

    public CustomSupportEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(!isInEditMode()) {
            init();
        }
    }

    private void init() {
        setTypeface(Utility.getRegularTypeFace());
        if(relatedTextInputLayout != null) {
            setOnFocusChangeListener(new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (hasFocus) {
                        if (getStatus() == ERROR_STATUS) {
                            relatedTextInputLayout.setError(errorText);
                        } else if (getStatus() == REQUIRE_STATE) {
                            relatedTextInputLayout.setError(requiredErrorText);
                        } else {
                            relatedTextInputLayout.setError(null);
                        }
                    } else {
                        if (getStatus() == ERROR_STATUS || getStatus() == DISABLE_ERROR_STATE || getStatus() == REQUIRE_STATE) {
                            relatedTextInputLayout.setError(" ");
                        } else {
                            relatedTextInputLayout.setError(null);
                        }
                    }
                }
            });
        }
    }

    public void init(int status, CustomTextInputLayout customTextInputLayout, String errorText, String requiredErrorText) {
        this.relatedTextInputLayout = customTextInputLayout;
        this.errorText = errorText;
        this.requiredErrorText = requiredErrorText;
        setStatus(status);
    }

    public boolean isDisable() {
        if(this.status == DISABLE_STATE || this.status == DISABLE_ERROR_STATE) {
            return true;
        }
        return false;
    }

    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    public String getRequiredErrorText() {
        return requiredErrorText;
    }

    public void setRequiredErrorText(String requiredErrorText) {
        this.requiredErrorText = requiredErrorText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;

        if(status == REQUIRE_STATE || status == ERROR_STATUS || status == DISABLE_ERROR_STATE) {
            relatedTextInputLayout.setError(" ");
        }
        else {
            relatedTextInputLayout.setError(null);
        }
    }

    public CustomTextInputLayout getRelatedTextInputLayout() {
        return relatedTextInputLayout;
    }

    public void setRelatedTextInputLayout(CustomTextInputLayout relatedTextInputLayout) {
        this.relatedTextInputLayout = relatedTextInputLayout;
    }

    public void setTextWithTypeFace(CharSequence text) {
//        if (Statics.LANGUAGE.equals(LanguageType.FA)) {
//            String convertedText = "";
//            for(int i=0; i<text.length(); i++) {
//                convertedText += Utility.convertToPersianDigits((text.charAt(i) + ""));
//                super.setText(convertedText);
//            }
//        }
//        else {
//            super.setText(text);
//        }
    }
}
