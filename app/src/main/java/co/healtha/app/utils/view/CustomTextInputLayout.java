package co.healtha.app.utils.view;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import android.util.AttributeSet;

import co.healtha.app.utils.Utility;

/**
 * Created by a.vatani on 1/25/17.
 */

public class CustomTextInputLayout extends TextInputLayout {

    public CustomTextInputLayout(Context context) {
        super(context);
        if(!isInEditMode()) {
            init();
        }
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()) {
            init();
        }
    }

    public CustomTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(!isInEditMode()) {
            init();
        }
    }

    private void init() {
        setTypeface(Utility.getRegularTypeFace());
    }

    @Override
    public void setError(@Nullable CharSequence error) {
        if(error != null) {
            SpannableString s = new SpannableString(error);
            s.setSpan(new TypefaceSpan(Utility.getRegularTypeFace()), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            super.setError(s);
        }
        else {
            super.setError(error);
        }
    }

    public class TypefaceSpan extends MetricAffectingSpan {

        private Typeface mTypeface;

        public TypefaceSpan(Typeface typeface) {
            mTypeface = typeface;
        }

        @Override
        public void updateMeasureState(TextPaint p) {
            p.setTypeface(mTypeface);
            p.setFlags(p.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
        }

        @Override
        public void updateDrawState(TextPaint tp) {
            tp.setTypeface(mTypeface);
            tp.setFlags(tp.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
        }
    }
}
