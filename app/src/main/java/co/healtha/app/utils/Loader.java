package co.healtha.app.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.widget.TextView;

public class Loader {

    private static ProgressDialog progressDialog;

//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static void showLoader(final Activity activity) {
        if (activity == null) {
            return;
        }
        dismissLoader();
        Utility.getUiHandler().post(new Runnable() {
            @Override
            public void run() {
                progressDialog = ProgressDialog.show(activity, "", "لطفا کمی صبر کنید...", true);

                TextView view = (TextView) progressDialog.findViewById(android.R.id.message);
                if (view != null)
                    view.setTypeface(Utility.getRegularTypeFace());
//                    progressDialog.setCancelable(true);
//                    progressDialog.setCanceledOnTouchOutside(true);
            }
        });
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static void dismissLoader() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        Utility.getUiHandler().post(new Runnable() {
            @Override
            public void run()
            {
                if (progressDialog != null)
                    progressDialog.dismiss();
            }
        });
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
}

