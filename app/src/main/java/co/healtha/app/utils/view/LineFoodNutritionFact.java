package co.healtha.app.utils.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import java.util.ArrayList;

import co.healtha.app.utils.Utility;

/**
 * Created by ahmad on 2/3/17.
 */

public class LineFoodNutritionFact extends View {

    private int height = 20;
    private int width;
    private int valueCount;
    private ArrayList<Integer> valueColors = new ArrayList<>();
    private ArrayList<Paint> valueBarPaints = new ArrayList<>();
    private ArrayList<Paint> valueTextPaints = new ArrayList<>();
    private ArrayList<Integer> values = new ArrayList<>();
    private float textSize;


    public LineFoodNutritionFact(Context context) {
        super(context);
        init(context, null);
    }

    public LineFoodNutritionFact(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public LineFoodNutritionFact(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public LineFoodNutritionFact(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }


    private void init(Context context, AttributeSet attrs) {

        textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                13, getResources().getDisplayMetrics());

        valueCount = 4;
        valueColors.add(Color.RED);
        valueColors.add(Color.BLACK);
        valueColors.add(Color.BLUE);
        valueColors.add(Color.GREEN);

        values.add(14);
        values.add(36);
        values.add(20);
        values.add(30);

        for (int i=0; i<valueCount; i++) {
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setColor(valueColors.get(i));
            paint.setStyle(Paint.Style.FILL);

            valueBarPaints.add(paint);

            paint.setTextSize(textSize);
            paint.setTypeface(Utility.getRegularTypeFace());

            valueTextPaints.add(paint);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int start = 0, end = 0;
        for (int i=0; i<valueCount; i++) {

            //draw bar
            end = start + values.get(i) * width / 100;
            RectF r = new RectF(start, 0, end, 20);
            canvas.drawRect(r, valueBarPaints.get(i));

            //draw text
            Paint textPaint = valueTextPaints.get(i);
            String text = "%" + values.get(i);
            float textWidth = textPaint.measureText(text);
            canvas.drawText(text, ((start + end) / 2) - (textWidth / 2), 70 , textPaint);

            start = end;
        }
    }



    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
    }
}
