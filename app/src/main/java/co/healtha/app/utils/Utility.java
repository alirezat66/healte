package co.healtha.app.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import co.healtha.app.R;

public class Utility {

    private static volatile Handler uiHandler;
    private static Context context;

//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static JSONObject getJsonObject(Object object) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonString = gson.toJson(object);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static boolean hasConnection() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }

        return false;
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static void setUiHandler(Looper looper) {
        uiHandler = new Handler(looper);
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static Handler getUiHandler() {
        return uiHandler;
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static void setContext(Context c) {
        context = c;
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static Context getContext() {
        return context;
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static void logout() {
        SharedPreferencesHelper.getInstance().setIsFirstTime(true);
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static Bitmap decodeURI(String filePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Only scale if we need to
        // (16384 buffer for img processing)
        Boolean scaleByHeight = Math.abs(options.outHeight - 100) >= Math.abs(options.outWidth - 100);
        if(options.outHeight * options.outWidth * 2 >= 16384){
            // Load, scaling to smallest power of 2 that'll get it <= desired dimensions
            double sampleSize = scaleByHeight
                    ? options.outHeight / 100
                    : options.outWidth / 100;
            options.inSampleSize =
                    (int) Math.pow(2d, Math.floor(
                            Math.log(sampleSize)/ Math.log(2d)));
        }

        // Do the actual decoding
        options.inJustDecodeBounds = false;
        options.inTempStorage = new byte[512];
        Bitmap output = BitmapFactory.decodeFile(filePath, options);

        return output;
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static String getDifferenceDate(String strDateTime) {
        String dateFormat = "yyyy-MM-dd'T'HH:mm:ss";
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        formatter.setTimeZone(TimeZone.getTimeZone("GMT+03:30"));
        Calendar nowCal = Calendar.getInstance();
        long nowTime = nowCal.getTimeInMillis();
        long time = 0;
        Date dateTime = null;
        Calendar calDateTime = Calendar.getInstance();
        try {
            calDateTime.setTimeZone(TimeZone.getTimeZone("GMT+03:30"));
            calDateTime.setTimeInMillis(formatter.parse(strDateTime).getTime());
            dateTime = formatter.parse(strDateTime);
            time = dateTime.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = nowTime - time;

        if(diff < 24 * 60 * 60 * 1000) {
            if(diff < 60 * 60 * 1000) {
                if(diff < 60 * 1000) {
                    if(diff < 0) {
                        return 0 + " " + context.getString(R.string.general_date_second_ago);
                    } else {
                        return (diff / 1000) + " " + context.getString(R.string.general_date_second_ago);
                    }
                } else {
                    return (diff / (60 * 1000)) + " " + context.getString(R.string.general_date_minute_ago);
                }
            } else {
                return (diff / (60 * 60 * 1000)) + " " + context.getString(R.string.general_date_hour_ago);
            }
        } else {
            JalaliCalendar jalaliCalendar= new JalaliCalendar();
            JalaliCalendar.YearMonthDate yearMonthDate = jalaliCalendar.gregorianToJalali(new JalaliCalendar.YearMonthDate(calDateTime.get(Calendar.YEAR), calDateTime.get(Calendar.MONTH), calDateTime.get(Calendar.DAY_OF_MONTH)));
            return yearMonthDate.getYear() + " " + JalaliCalendar.getPersianMonthName(yearMonthDate.getMonth()) + " " + yearMonthDate.getDate();
        }
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static String getDateFromString(String strDateTime) {
        String dateFormat = "yyyy-MM-dd'T'HH:mm:ss";
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        formatter.setTimeZone(TimeZone.getTimeZone("GMT+03:30"));
        Calendar calDateTime = Calendar.getInstance();
        try {
            calDateTime.setTimeZone(TimeZone.getTimeZone("GMT+03:30"));
            calDateTime.setTimeInMillis(formatter.parse(strDateTime).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        JalaliCalendar jalaliCalendar= new JalaliCalendar();
        JalaliCalendar.YearMonthDate yearMonthDate = jalaliCalendar.gregorianToJalali(new JalaliCalendar.YearMonthDate(calDateTime.get(Calendar.YEAR), calDateTime.get(Calendar.MONTH), calDateTime.get(Calendar.DAY_OF_MONTH)));
        return yearMonthDate.getYear() + " " + JalaliCalendar.getPersianMonthName(yearMonthDate.getMonth()) + " " + yearMonthDate.getDate();
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static File getTempFile() {
        //it will return /sdcard/image.tmp
        final File path = new File(Environment.getExternalStorageDirectory(), ".temp");
        if(!path.exists()) {
            path.mkdir();
        }
        return new File(path, "image.jpeg");
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static Typeface getRegularTypeFace() {
        return Typeface.createFromAsset(getContext().getAssets(), "fonts/IRAN-Sans-Regular.ttf");
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static Typeface getBoldTypeFace() {
        return Typeface.createFromAsset(getContext().getAssets(), "fonts/IRAN-Sans-Medium.ttf");
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static String convertToEnglishDigits(String value) {
        return value.replace("١", "1").replace("٢", "2").replace("٣", "3").replace("٤", "4").replace("٥", "5")
                .replace("٦", "6").replace("٧", "7").replace("٨", "8").replace("٩", "9").replace("٠", "0")
                .replace("۱", "1").replace("۲", "2").replace("۳", "3").replace("۴", "4").replace("۵", "5")
                .replace("۶", "6").replace("۷", "7").replace("۸", "8").replace("۹", "9").replace("۰", "0");
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static String convertToPersianDigits(String value) {
        return value.replace("1", "١").replace("2", "٢").replace("3", "٣").replace("4", "۴").replace("5", "۵")
                .replace("6", "۶").replace("7", "٧").replace("8", "٨").replace("9", "٩").replace("0","٠");
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                context.getResources().getDisplayMetrics());
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static int sp2px(float sp) {
        return  (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static void applyFontToMenuItem(MenuItem mi) {
        Typeface font = getRegularTypeFace();
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new MenuTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static long expand(final View v) {
        v.measure(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();
        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? RelativeLayout.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        long duration = (int) (targetHeight * 2 / v.getContext().getResources().getDisplayMetrics().density);
        a.setDuration(duration);
        v.startAnimation(a);

        return duration;
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static void expand(final View v, long duration) {
        v.measure(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();
        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? RelativeLayout.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration(duration);
        v.startAnimation(a);
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static long collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        long duration = (int) (initialHeight * 2 / v.getContext().getResources().getDisplayMetrics().density);
        a.setDuration(duration);
        v.startAnimation(a);

        return duration;
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static void collapse(final View v, long duration) {
        final int initialHeight = v.getMeasuredHeight();
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration(duration);
        v.startAnimation(a);
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static int hex2rgb(String hex) {
        int color = (int) Long.parseLong(hex.replace("#", ""), 16);
        int r = (color >> 16) & 0xFF;
        int g = (color >> 8) & 0xFF;
        int b = (color >> 0) & 0xFF;
        return Color.rgb(r, g, b);
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static String getPriceFormat(int price) {
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(price);
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
     private static boolean isServiceRunning(String service_name) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (service_name.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static void setDirection(Activity activity) {
        //TODO this method should delete in future
        activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        Configuration configuration = activity.getResources().getConfiguration();
        configuration.setLayoutDirection(new Locale("fa"));
        activity.getResources().updateConfiguration(configuration, activity.getResources().getDisplayMetrics());
    }

}

