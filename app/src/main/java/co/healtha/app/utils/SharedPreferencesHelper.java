package co.healtha.app.utils;

import android.content.SharedPreferences;

/**
 * Created by ahmad on 1/20/17.
 */

public class SharedPreferencesHelper {

    public static final String PREFERENCES_NAME_SETTINGS = "settings";
    public static final String PREFERENCES_SETTINGS_IS_FIRST_LAUNCH = "is_first_launch";
    public static final String PREFERENCES_SETTINGS_FONT_SIZE_SCALE = "font_size_scale";

    private static SharedPreferencesHelper instance = new SharedPreferencesHelper();
    private SharedPreferences prefSettings;
    private SharedPreferences.Editor editorSettings;

//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public static SharedPreferencesHelper getInstance() {
        return instance;
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    private SharedPreferencesHelper() {
        prefSettings = Utility.getContext().getSharedPreferences(PREFERENCES_NAME_SETTINGS, Utility.getContext().MODE_PRIVATE);
        editorSettings = prefSettings.edit();
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public void setIsFirstTime(boolean isFirstTime) {
        editorSettings.putBoolean(PREFERENCES_SETTINGS_IS_FIRST_LAUNCH, isFirstTime);
        editorSettings.apply();
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public boolean getIsFirstTime() {
        return prefSettings.getBoolean(PREFERENCES_SETTINGS_IS_FIRST_LAUNCH, true);
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public float getFontSizeScale() {
        return prefSettings.getFloat(PREFERENCES_SETTINGS_FONT_SIZE_SCALE, 1f);
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
    public void setFontSizeScale(float scale) {
        editorSettings.putFloat(PREFERENCES_SETTINGS_FONT_SIZE_SCALE, scale);
        editorSettings.apply();
    }
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *
//* # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # * # *

}
