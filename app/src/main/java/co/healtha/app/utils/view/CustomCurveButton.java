package co.healtha.app.utils.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.Button;

import co.healtha.app.utils.Utility;

/**
 * Created by ahmad on 1/16/17.
 */

public class CustomCurveButton extends Button {

    public CustomCurveButton(Context context) {
        super(context);
        if(!isInEditMode()) {
            init();
        }
    }

    public CustomCurveButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode()) {
            init();
        }
    }

    public CustomCurveButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(!isInEditMode()) {
            init();
        }
    }

    public CustomCurveButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if(!isInEditMode()) {
            init();
        }
    }

    private void init() {

        setTypeface(Utility.getRegularTypeFace());

        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(15);
        ColorDrawable buttonColor = (ColorDrawable) getBackground();
        shape.setColor(buttonColor.getColor());
        setBackground(shape);

    }
}
