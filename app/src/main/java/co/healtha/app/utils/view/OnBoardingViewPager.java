package co.healtha.app.utils.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by ahmad on 1/16/17.
 */

public class OnBoardingViewPager  extends ViewPager {

    public OnBoardingViewPager(Context context) {
        super(context);
    }

    public OnBoardingViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
