package co.healtha.app.utils;

import android.util.Log;

public class L {

    public static void i(String log)
    {
        Log.i(Statics.TAG, log);
    }

    public static void e(String log, Throwable throwable)
    {
        Log.e(Statics.TAG, log, throwable);
    }

    public static void e(String log)
    {
        Log.e(Statics.TAG, log);
    }
}
