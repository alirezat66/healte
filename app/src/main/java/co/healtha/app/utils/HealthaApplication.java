package co.healtha.app.utils;

import android.app.Application;
import java.io.File;

/**
 * Created by ahmad on 1/20/17.
 */

public class HealthaApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Utility.setUiHandler(getMainLooper());
        Utility.setContext(getApplicationContext());

        File f;
        f = new File(Statics.SHOP_DIRECTORY);
        if(!f.exists()) {
            f.mkdir();
        }

        f = new File(Statics.CACHE_DIRECTORY);
        if(!f.exists()) {
            f.mkdir();
        }
    }
}
